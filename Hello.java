import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Hello {

	
	public static void main(String[] args) throws FileNotFoundException{
	    /*
	    Parse the output and generate XML file.. Changed content again..
	    */
		Scanner scanner=new Scanner(System.in);
		String siteOs=scanner.nextLine();
		Scanner tmp = new Scanner(siteOs);
		String site = tmp.next();
		String os = tmp.next();
		File f=new File(site+"-"+os+"-release.txt");
		Scanner s=new Scanner(f);
		String name="", version="", IP="", next="", previous="";
		PrintWriter pw=new PrintWriter(site + "-" + os+"-output.csv");
		
		while (s.hasNextLine()) {
			name=version=IP=next=previous="";
			String test=s.nextLine();
			Scanner in=new Scanner(test);
			//System.out.println(test);
			name=in.next();
			while (in.hasNext()) {
				previous=next;
				next=in.next();
				if (next.equals("release") || next.equals("Solaris")) version=in.next();
				if (Pattern.matches("^10.2[0-9]+.[0-9]+.[0-9]+", next)) IP=next;
			}
			//IP=previous;
			in.close();
			if(Pattern.matches("[5-7].[0-9]+", version) || Pattern.matches("10", version) || Pattern.matches("11.[0-9]+", version)){
				pw.println(name+","+os+","+IP+","+version);
			}
			else{
				if(Pattern.matches("^10.2[0-9]+.[0-9]+.[0-9]+", IP)){
					pw.println(name+","+os+","+IP);
				}
				else {
					pw.println(name+","+os);
				}
			}
		}
		s.close();
		pw.close();
	}
}
